package com.example.espino.ejerciciosficheros.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.espino.ejerciciosficheros.R;
import com.example.espino.ejerciciosficheros.models.Contact;
import com.example.espino.ejerciciosficheros.models.Diary;


public class DiaryAdapter extends ArrayAdapter<Contact> {

    private Context context;

    public DiaryAdapter(Context context) {
        super(context, R.layout.listitem_contact, Diary.getInstance(context).getDiary());
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View item = convertView;
        ContactHolder holder;


        if(item == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            item = inflater.inflate(R.layout.listitem_contact,null);
            holder = new ContactHolder();

            holder.name = (TextView) item.findViewById(R.id.listitem_contact_name);
            holder.phone = (TextView) item.findViewById(R.id.listitem_contact_phone);
            holder.email = (TextView) item.findViewById(R.id.listitem_contact_email);

            item.setTag(holder);
        }
        else
            holder = (ContactHolder) item.getTag();

            holder.name.setText(getItem(position).getName());
            holder.phone.setText(getItem(position).getPhone());
            holder.email.setText(getItem(position).getEmail());


        return item;
    }

    class ContactHolder{
        private TextView name,phone,email;
    }
}
