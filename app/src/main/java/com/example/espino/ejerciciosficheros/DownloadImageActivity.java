package com.example.espino.ejerciciosficheros;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.espino.ejerciciosficheros.HTTPmehtods.RestClient;
import com.example.espino.ejerciciosficheros.fileMethods.FileInputMethods;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;


import cz.msebera.android.httpclient.Header;


public class DownloadImageActivity extends AppCompatActivity {

    private ImageView images;
    private EditText address;
    private String[] urls;
    private byte index;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloadimages);

        images = (ImageView) findViewById(R.id.downloadimage_imageview);
        address = (EditText) findViewById(R.id.downloadimage_edt_url);


        index = 0;
    }

    public void downloadImages(View v) {

        if (!TextUtils.isEmpty(address.getText().toString()))
            aahc(address.getText().toString());
    }

    public void move(View v) {
        switch (v.getId()) {
            case R.id.downloadimage_btn_down:
                if (index + 1 < urls.length) {
                    Picasso.with(getApplicationContext())
                            .load(urls[++index])
                            .placeholder(R.drawable.wait)
                            .error(R.drawable.error)
                            .resize(1024, 720)
                            .into(images);
                }
                break;

            case R.id.downloadimage_btn_up:
                if (index - 1 >= 0) {
                    Picasso.with(getApplicationContext())
                            .load(urls[--index])
                            .placeholder(R.drawable.wait)
                            .error(R.drawable.error)
                            .resize(1024, 720)
                            .into(images);
                }
                break;
        }
    }

    private void aahc(String url) {
        final ProgressDialog progress = new ProgressDialog(this);

        RestClient.get(url, new TextHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setMessage("Conectando . . .");
                progress.setCancelable(false);
                progress.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progress.dismiss();
                Toast.makeText(getApplicationContext(), "Fallo: " + throwable.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                progress.dismiss();
                urls = responseString.split("\n");
                if (urls.length > 0) {
                    Picasso.with(getApplicationContext())
                            .load(urls[index])
                            .placeholder(R.drawable.wait)
                            .error(R.drawable.error)
                            .resize(1024, 720)
                            .into(images);
                    Toast.makeText(getApplicationContext(), "Descarga correcta", Toast.LENGTH_LONG).show();

                }

            }
        });
    }
}
