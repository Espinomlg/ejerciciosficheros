package com.example.espino.ejerciciosficheros.HTTPmehtods;


import com.example.espino.ejerciciosficheros.models.Result;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class JavaConnection {

    public static Result connectWithJava(String texto) {
        URL url;
        HttpURLConnection urlConnection = null;
        int respuesta;
        Result result = new Result();
        try {
            url = new URL(texto);
            urlConnection = (HttpURLConnection) url.openConnection();
            respuesta = urlConnection.getResponseCode();
            if (respuesta == HttpURLConnection.HTTP_OK) {
                result.setCode(true);
                result.setContent(leer(urlConnection.getInputStream()));
            } else {
                result.setCode(false);
                result.setMessage("Error en el acceso a la web: " + String.valueOf(respuesta));
            }
        } catch (IOException e) {
            result.setCode(false);
            result.setMessage("Excepción: " + e.getMessage());
        } finally {
            try {
                if (urlConnection != null)
                    urlConnection.disconnect();
            } catch (Exception e) {
                result.setCode(false);
                result.setMessage("Excepción: " + e.getMessage());
            }
            return result;
        }
    }


    private static String leer(InputStream entrada) throws IOException{
        BufferedReader in;
        String linea;
        StringBuilder miCadena = new StringBuilder();
        in = new BufferedReader(new InputStreamReader(entrada), 32000);
        while ((linea = in.readLine()) != null)
            miCadena.append(linea);

        in.close();
        return miCadena.toString();
    }

}
