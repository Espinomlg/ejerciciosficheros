package com.example.espino.ejerciciosficheros.models;


import java.io.Serializable;

public class Alarm implements Serializable{

    private int time;
    private String text;

    public Alarm(int time, String text){
        this.time = time;
        this.text = text;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
