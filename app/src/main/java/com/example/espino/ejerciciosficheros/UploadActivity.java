package com.example.espino.ejerciciosficheros;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.espino.ejerciciosficheros.HTTPmehtods.RestClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.File;
import java.io.FileNotFoundException;

import cz.msebera.android.httpclient.Header;


public class UploadActivity extends AppCompatActivity {

    private TextView fileName, information;
    public static final int UPLOAD_FILE_CODE = 1;
    public static final String WEB = "http://192.168.1.10/acdat/upload.php";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uploadfile);

        fileName = (TextView) findViewById(R.id.uploadfile_txv_file);
        information = (TextView) findViewById(R.id.uploadfile_txv_information);
    }

    public void upload(View v){
        uploadFile();
    }

    public void selectFile (View v) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("file/*");
        if (intent.resolveActivity(getPackageManager()) != null)
            startActivityForResult(intent, UPLOAD_FILE_CODE);
        else
            Toast.makeText(this, "No hay aplicación para manejar ficheros", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        if (requestCode == UPLOAD_FILE_CODE)
            if (resultCode == RESULT_OK) {

                String ruta = data.getData().getPath();
                fileName.setText(ruta);
            }
            else
                Toast.makeText(this, "Error: " + resultCode, Toast.LENGTH_SHORT).show();
    }

    private void uploadFile() {
        String file = fileName.getText().toString();
        final ProgressDialog progreso = new ProgressDialog(UploadActivity.this);
        File myFile;
        Boolean existe = true;
        myFile = new File(fileName.getText().toString());
        RequestParams params = new RequestParams();
        try {
            params.put("fileToUpload", myFile);
            params.put("password", "1234");
        } catch (FileNotFoundException e) {
            existe = false;
            information.setText("Error en el fichero: " + e.getMessage());
        }
        if (existe)
            RestClient.post(WEB, params, new TextHttpResponseHandler() {
                @Override
                public void onStart() {
                    progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progreso.setMessage("Conectando . . .");
                    progreso.setCancelable(false);
                    progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                            RestClient.cancelRequests(getApplicationContext(), true);
                        }
                    });
                    progreso.show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String response) {
                    progreso.dismiss();
                    information.setText(response);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String response, Throwable t) {
                    progreso.dismiss();
                    information.setText("Fallo: "+ statusCode+ "\n" + response + "\n");
                }
            });
    }
}
