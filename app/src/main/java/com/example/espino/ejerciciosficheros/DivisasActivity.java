package com.example.espino.ejerciciosficheros;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.espino.ejerciciosficheros.HTTPmehtods.RestClient;
import com.example.espino.ejerciciosficheros.fileMethods.FileInputMethods;
import com.example.espino.ejerciciosficheros.models.Files;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import cz.msebera.android.httpclient.Header;


public class DivisasActivity extends AppCompatActivity {

    private EditText editDolares;
    private EditText editEuros;
    private RadioButton dolarAeuro;
    private RadioButton euroAdolar;


    private double cambio;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_divisas);

        editDolares = (EditText) findViewById(R.id.Dollarstext);
        editEuros = (EditText) findViewById(R.id.EurosText);
        dolarAeuro = (RadioButton) findViewById(R.id.dolarAeuro);
        euroAdolar = (RadioButton) findViewById(R.id.euroAdolar);

        cambio = 0;
    }

    public void getEvent(View view)
    {
        switch (view.getId())
        {
            case R.id.dolarAeuro:
                dolarAeuro.setChecked(true);
                euroAdolar.setChecked(false);
                break;

            case R.id.euroAdolar:
                dolarAeuro.setChecked(false);
                euroAdolar.setChecked(true);
                break;

            case R.id.button:

                aahc(Files.URL_FILE);
                if(cambio != 0) {
                    if (dolarAeuro.isChecked() && editDolares.getText().length() > 0)
                        editEuros.setText(convertirAeuros(editDolares.getText().toString()));

                    else if (euroAdolar.isChecked() && editEuros.getText().length() > 0)
                        editDolares.setText(convertirAdolares(editEuros.getText().toString()));
                }
                break;
        }
    }

    private String convertirAeuros(String cantidad)
    {
        double valor = Double.parseDouble(cantidad) / cambio;

        return String.format(Locale.US,"%.2f",valor);
    }

    private String convertirAdolares(String cantidad)
    {
        double valor = Double.parseDouble(cantidad) * cambio;

        return String.format(Locale.US,"%.2f",valor);
    }

    private void aahc(String url) {
        final ProgressDialog progress = new ProgressDialog(this);

        RestClient.get(url, new TextHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setMessage("Conectando . . .");
                progress.setCancelable(false);
                progress.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progress.dismiss();
                Toast.makeText(getApplicationContext(), "Fallo: " + throwable.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                progress.dismiss();
                try {
                    cambio = Double.valueOf(responseString);
                    Toast.makeText(getApplicationContext(), "Descarga correcta", Toast.LENGTH_LONG).show();
                }
                catch (NumberFormatException e){
                    Toast.makeText(getApplicationContext(), "Error: El archivo no contiene un valor numérico \n" + responseString, Toast.LENGTH_LONG).show();
                }


            }
        });
    }
}
