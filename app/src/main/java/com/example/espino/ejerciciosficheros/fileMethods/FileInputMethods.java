package com.example.espino.ejerciciosficheros.fileMethods;


import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.example.espino.ejerciciosficheros.models.Files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class FileInputMethods {

    private Context context;

    public FileInputMethods(Context context){
        this.context = context;
    }

    public Object readInternal(String file) {
        File myfile;
        myfile = new File(context.getFilesDir(), file);

        return read(myfile);
    }

    private Object read(File file){
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        Object obj = null;

        try {
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            obj = ois.readObject();

        } catch (IOException e) {
            Log.e("Error", e.getMessage());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                ois.close();
            } catch (IOException e) {
                Log.e("Error al cerrar", e.getMessage());
            }
        }
        return obj;
    }

    public Object readExternal(String file){
        File myFile, sdcard;
        sdcard = Environment.getExternalStorageDirectory();
        myFile = new File(sdcard.getAbsolutePath(), file);

        return read(myFile);
    }

    public boolean externalStorageAvailable(){
        boolean exists = false;

        //Comprobamos el estado de la memoria externa (tarjeta SD)
        String state = Environment.getExternalStorageState();

        if (state.equals(Environment.MEDIA_MOUNTED))
            exists = true;

        return exists;
    }


}
