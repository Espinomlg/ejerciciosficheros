package com.example.espino.ejerciciosficheros.models;

/**
 * Created by espino on 17/11/16.
 */

public class Files {

    public static final String DIARY_FILE = "diary.txt";
    public static final String CHARSET = "UTF-8";
    public static final String ALARMS_FILE = "alarmas.txt";
    public static final String FERTILDAYS_FILE = "fertildays.txt";
    public static final String URL_FILE = "http://192.168.1.10/acdat/cambio.txt";
}
