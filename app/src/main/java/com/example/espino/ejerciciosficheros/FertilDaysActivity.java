package com.example.espino.ejerciciosficheros;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.example.espino.ejerciciosficheros.fileMethods.FileOutputMethods;
import com.example.espino.ejerciciosficheros.models.Files;
import com.example.espino.ejerciciosficheros.models.PeriodDate;

import java.io.File;
import java.util.Calendar;
import java.util.GregorianCalendar;


public class FertilDaysActivity extends AppCompatActivity {

    private DatePicker lastDay;
    private EditText edtDuration;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fertildays);



        lastDay = (DatePicker) findViewById(R.id.fertildays_datepicker_lastday);
        edtDuration = (EditText) findViewById(R.id.fertildays_edt_duration);

        setDatePicker(lastDay);
    }

    private void setDatePicker(DatePicker view) {

        Calendar now = Calendar.getInstance();
        now.add(Calendar.MONTH, 1);
        view.setMaxDate(now.getTimeInMillis());
        now.add(Calendar.MONTH, -2);
        view.setMinDate(now.getTimeInMillis());

    }

    public void showFertilDays(View v) {
        FileOutputMethods fom = new FileOutputMethods(FertilDaysActivity.this);

        if (fom.externalStorageAvailable()) {
            String last = "" + lastDay.getDayOfMonth() + "/" + lastDay.getMonth() + "/" + lastDay.getYear();

            PeriodDate pd = new PeriodDate(last, Integer.valueOf(edtDuration.getText().toString()));


            fom.writeExternal(Files.FERTILDAYS_FILE, pd, false, Files.CHARSET);
            startActivity(new Intent(FertilDaysActivity.this, ShowFertilDaysActivity.class));
        } else
            Toast.makeText(FertilDaysActivity.this, "Hay un problema con la memoria externa", Toast.LENGTH_SHORT).show();

    }
}
