package com.example.espino.ejerciciosficheros;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.espino.ejerciciosficheros.fileMethods.FileOutputMethods;
import com.example.espino.ejerciciosficheros.models.Contact;
import com.example.espino.ejerciciosficheros.models.Diary;
import com.example.espino.ejerciciosficheros.models.Files;

/**
 * Created by espino on 17/11/16.
 */

public class AddContactActivity extends AppCompatActivity{

    private TextInputLayout txiName,
    txiPhone,
    txiEmail;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addcontact);

        txiName = (TextInputLayout) findViewById(R.id.addcontact_txi_name);
        txiPhone = (TextInputLayout) findViewById(R.id.addcontact_txi_phone);
        txiEmail = (TextInputLayout) findViewById(R.id.addcontact_txi_email);
    }

    public void add(View v){
        Contact c = new Contact(txiName.getEditText().getText().toString(),
                txiPhone.getEditText().getText().toString(),
                txiEmail.getEditText().getText().toString());

        Diary.getInstance(AddContactActivity.this).add(c);
        FileOutputMethods fom = new FileOutputMethods(AddContactActivity.this);
        fom.writeInternal(Files.DIARY_FILE,Diary.getInstance(AddContactActivity.this).getDiary(),false,Files.CHARSET);
        finish();
    }
}
