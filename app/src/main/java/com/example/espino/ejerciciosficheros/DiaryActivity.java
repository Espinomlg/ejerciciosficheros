package com.example.espino.ejerciciosficheros;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import com.example.espino.ejerciciosficheros.adapters.DiaryAdapter;



public class DiaryActivity extends ListActivity {

    private DiaryAdapter adapter;
    private FloatingActionButton btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diary);

        adapter = new DiaryAdapter(DiaryActivity.this);
        getListView().setAdapter(adapter);
        btn = (FloatingActionButton) findViewById(R.id.diary_floatingbtn);
    }

    public void addContact(View v){
        startActivity(new Intent(DiaryActivity.this, AddContactActivity.class));
    }
}
