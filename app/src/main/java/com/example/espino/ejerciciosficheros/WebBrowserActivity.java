package com.example.espino.ejerciciosficheros;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.espino.ejerciciosficheros.HTTPmehtods.JavaConnection;
import com.example.espino.ejerciciosficheros.HTTPmehtods.RestClient;
import com.example.espino.ejerciciosficheros.models.RequestQueueSingleton;
import com.example.espino.ejerciciosficheros.models.Result;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.File;


public class WebBrowserActivity extends AppCompatActivity {

    private EditText address, fileName;
    private WebView web;
    private RadioGroup rdgroup;
    public static final String TAG = "Volley";
    private RequestQueue req;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webbrowser);

        req = RequestQueueSingleton.getInstance(WebBrowserActivity.this).getRequestQueue();

        address = (EditText) findViewById(R.id.webbrowser_edt_url);
        fileName = (EditText) findViewById(R.id.webbrowser_edt_filename);
        web = (WebView) findViewById(R.id.webbrowser_webview);
        rdgroup = (RadioGroup) findViewById(R.id.webbrowser_rdgroup);
    }



    private boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    public void connect(View v) {

        switch (v.getId()) {
            case R.id.webbrowser_btn_connect:
                if (!TextUtils.isEmpty(address.getText().toString()) && isNetworkAvailable()) {

                    switch (rdgroup.getCheckedRadioButtonId()) {
                        case R.id.webbrowser_rdbtn_java:
                            AsyncConnection connection = new AsyncConnection(WebBrowserActivity.this);
                            connection.execute(address.getText().toString());
                            break;
                        case R.id.webbrowser_rdbtn_aahc:
                            aahc(address.getText().toString());
                            break;
                        case R.id.webbrowser_rdbtn_volley:
                            makeRequestVolley(address.getText().toString());
                            break;
                    }
                } else
                    Toast.makeText(WebBrowserActivity.this, "No hay conexión a internet", Toast.LENGTH_SHORT).show();

                break;

            case R.id.webbrowser_btn_savefile:
                if(Environment.getExternalStorageState().compareTo(Environment.MEDIA_MOUNTED) == 0 &&
                        !TextUtils.isEmpty(fileName.getText().toString())) {
                    File file = new File(Environment.getExternalStorageDirectory(), fileName.getText().toString());
                    web.saveWebArchive(file.getAbsolutePath());
                    Toast.makeText(WebBrowserActivity.this, "Se ha guardado con éxito",Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(WebBrowserActivity.this, "Ha ocurrido un error",Toast.LENGTH_SHORT).show();

                break;
        }
    }

    private void aahc(String url) {
        final ProgressDialog progreso = new ProgressDialog(WebBrowserActivity.this);
        RestClient.get(url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        RestClient.cancelRequests(getApplicationContext(), true);
                    }
                });
                progreso.show();
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                progreso.dismiss();
                web.loadDataWithBaseURL(null, "fallo: " + responseString, "text/html", "UTF-8", null);
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                // called when response HTTP status is "200"
                progreso.dismiss();
                web.loadDataWithBaseURL(null, responseString, "text/html", "UTF-8", null);
            }
        });

    }

    public void makeRequestVolley(String url) {

        final String enlace = url;

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        web.loadDataWithBaseURL(enlace, response, "text/html", "utf-8", null);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String message = "";
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            message = "Timeout Error " + error.getMessage();
                        } else if (error instanceof AuthFailureError) {
                            message = "AuthFailure Error " + error.getMessage();
                        } else if (error instanceof ServerError) {
                            message = "Server Error " + error.getMessage();
                        } else if (error instanceof NetworkError) {
                            message = "Network Error " + error.getMessage();
                        } else if (error instanceof ParseError) {
                            message = "Parse Error " + error.getMessage();
                        }
                        web.loadDataWithBaseURL(null, message, "text/html", "utf-8", null);
                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
        req.add(stringRequest);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (req != null) {
            req.cancelAll(TAG);
        }
    }



    public class AsyncConnection extends AsyncTask<String, Integer, Result> {
        private ProgressDialog progress;
        private Context context;

        public AsyncConnection(Context context) {
            this.context = context;
        }

        protected void onPreExecute() {
            progress = new ProgressDialog(context);
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setMessage("Conectando . . .");
            progress.setCancelable(true);
            progress.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    AsyncConnection.this.cancel(true);
                }
            });
            progress.show();
        }

        protected Result doInBackground(String... cadena) {
            Result result;
            int i = 1;
            try {
                // operaciones en el hilo secundario
                publishProgress(i++);
                result = JavaConnection.connectWithJava(cadena[0]);

            } catch (Exception e) {

                result = new Result();
                result.setCode(false);
                result.setContent(e.getMessage());
            }
            return result;
        }

        protected void onProgressUpdate(Integer... progress) {
            this.progress.setMessage("Conectando " + Integer.toString(progress[0]));
        }

        protected void onPostExecute(Result result) {
            progress.dismiss();

            if (result.isCode())
                web.loadDataWithBaseURL(null, result.getContent(), "text/html", "UTF-8", null);
            else
                web.loadDataWithBaseURL(null, result.getMessage(), "text/html", "UTF-8", null);
        }

        protected void onCancelled() {
            progress.dismiss();

            web.loadDataWithBaseURL(null, "Cancerado", "text/html", "UTF-8", null);

        }
    }


}
