package com.example.espino.ejerciciosficheros;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.espino.ejerciciosficheros.fileMethods.FileInputMethods;
import com.example.espino.ejerciciosficheros.models.Files;
import com.example.espino.ejerciciosficheros.models.PeriodDate;

import java.util.Calendar;
import java.util.GregorianCalendar;


public class ShowFertilDaysActivity extends AppCompatActivity {

    private TextView information;
    private PeriodDate pd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showfertildays);

        information = (TextView) findViewById(R.id.showfertildays_txv_information);

        FileInputMethods fim = new FileInputMethods(ShowFertilDaysActivity.this);

        pd = (PeriodDate) fim.readExternal(Files.FERTILDAYS_FILE);
        calculateDays();
    }

    private void calculateDays(){
        String cadena = "Tus días fértiles son: \n";
        String[] lastday = pd.getLastDay().split("/");
        int media = pd.getDuration() / 2;

        Calendar firstDay = new GregorianCalendar(Integer.valueOf(lastday[2]),Integer.valueOf(lastday[1]),Integer.valueOf(lastday[0]));
        Calendar lastDay = new GregorianCalendar(Integer.valueOf(lastday[2]),Integer.valueOf(lastday[1]),Integer.valueOf(lastday[0]));
        lastDay.add(Calendar.DAY_OF_MONTH, media + 3);
        firstDay.add(Calendar.DAY_OF_MONTH, media - 3);

        for(byte cont = 0; cont < 7; cont++){
            cadena += firstDay.get(Calendar.DAY_OF_MONTH) + " de " + getMonth(firstDay.get(Calendar.MONTH)) + " del año " + firstDay.get(Calendar.YEAR) + "\n";
            firstDay.add(Calendar.DAY_OF_MONTH, 1);
        }

        if(Calendar.getInstance().after(firstDay) && Calendar.getInstance().before(lastDay))
            cadena += "Felicidades estás en tus días fértiles";

        information.setText(cadena);

    }

    private String getMonth(int field){
        switch(field){
            case 0:
                return "Enero";
            case 1:
                return "Febrero";
            case 2:
                return "Marzo";
            case 3:
                return "Abril";
            case 4:
                return "Mayo";
            case 5:
                return "Junio";
            case 6:
                return "Julio";
            case 7:
                return "Agosto";
            case 8:
                return "Septiembre";
            case 9:
                return "Octubre";
            case 10:
                return "Noviembre";
            case 11:
                return "Diciembre";

        }
        return "";
    }
}
