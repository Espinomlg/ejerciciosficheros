package com.example.espino.ejerciciosficheros;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.espino.ejerciciosficheros.fileMethods.FileInputMethods;
import com.example.espino.ejerciciosficheros.fileMethods.FileOutputMethods;
import com.example.espino.ejerciciosficheros.models.Alarm;
import com.example.espino.ejerciciosficheros.models.Files;

import java.io.EOFException;
import java.io.File;


public class AlarmsActivity extends AppCompatActivity {

    private EditText time,
    text;
    private TextView information;
    private LinearLayout alarmslayout;
    private MediaPlayer audio;
    private Button btnadd, btnstart;

    private static File file;
    private Alarm[] alarms;
    private byte cont, nalarm;
    private boolean running;
    private MyCountDownTimer crono;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarms);

        init();
        FileInputMethods fim = new FileInputMethods(AlarmsActivity.this);

        if(file.length() > 0) {
            alarms = (Alarm[]) fim.readExternal(Files.ALARMS_FILE);
            for(byte index = 0; index < alarms.length; index++){
                if(alarms[index] != null){
                    TextView txv = new TextView(AlarmsActivity.this);
                    txv.setText(alarms[index].getTime() + "->" + alarms[index].getText());
                    alarmslayout.addView(txv);
                    cont++;
                }
            }
        }
        else{
            alarms = new Alarm[5];
        }

    }

    public void onClick(View v){

        switch(v.getId()){
            case R.id.alarms_btn_add:
                FileOutputMethods fom = new FileOutputMethods(AlarmsActivity.this);
                String alarmTime = time.getText().toString(),
                        alarmText = text.getText().toString();
                if(!TextUtils.isEmpty(alarmTime) && alarmTime.matches("\\d+") || !TextUtils.isEmpty(alarmText)){
                    if(cont < alarms.length) {

                        if(fom.externalStorageAvailable()) {
                            alarms[cont] = new Alarm(Integer.valueOf(alarmTime), alarmText);
                            fom.writeExternal(Files.ALARMS_FILE, alarms, true, Files.CHARSET);
                            cont++;

                            TextView txv = new TextView(AlarmsActivity.this);
                            txv.setText(alarmTime + " -> " + alarmText);
                            alarmslayout.addView(txv);
                        }

                    }
                    else
                        Toast.makeText(AlarmsActivity.this, "Has llegado al límite de alarmas(5)",Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.alarms_btn_start:
                if(!running) {
                    disableViews();
                    crono = new MyCountDownTimer(alarms[0].getTime() * 60 * 1000, 1);
                    crono.start();
                    running = !running;
                    btnstart.setText("Parar");
                }
                else {
                    crono.cancel();
                    btnstart.setText("Comenzar");
                    running = !running;
                }
                break;

            case R.id.alarms_btn_reset:
                if(file.delete())
                    Toast.makeText(AlarmsActivity.this,"Las alarmas se han borrado con exito",Toast.LENGTH_SHORT).show();
                for(byte index = 0; index < alarmslayout.getChildCount(); index++){
                    alarmslayout.removeViewAt(index);
                    alarmslayout.refreshDrawableState();
                }
                cont = 0;
                break;
        }
    }

    private void init(){
        time = (EditText) findViewById(R.id.alarms_edt_time);
        text = (EditText) findViewById(R.id.alarms_edt_txt);
        information = (TextView) findViewById(R.id.alarms_txv_information);
        btnadd = (Button) findViewById(R.id.alarms_btn_add);
        btnstart = (Button) findViewById(R.id.alarms_btn_start);
        alarmslayout = (LinearLayout) findViewById(R.id.alarms_linearlayout);
        audio = MediaPlayer.create(AlarmsActivity.this, R.raw.alarma);

        file = new File(Environment.getExternalStorageDirectory(), Files.ALARMS_FILE);

        cont = 0;
        nalarm = 0;
        running = false;
        crono = null;
    }

    private void disableViews(){
        time.setEnabled(false);
        text.setEnabled(false);
        btnadd.setEnabled(false);
    }

    private class MyCountDownTimer extends CountDownTimer{


        private MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            long minutesUntilFinished = millisUntilFinished / 1000 / 60;
            long secondsUntilFinished = millisUntilFinished / 1000 % 60;
            information.setText("" + minutesUntilFinished + ":" + secondsUntilFinished + "\nalarmas restantes: " + ((cont - nalarm) - 1));
        }

        @Override
        public void onFinish() {
            audio.start();
            Toast.makeText(AlarmsActivity.this,alarms[nalarm].getText(),Toast.LENGTH_LONG).show();
            nalarm++;
            if(nalarm < cont) {
                crono = new MyCountDownTimer(alarms[nalarm].getTime() * 60 * 1000, 1);
                crono.start();
            }
            else{
                btnstart.setText("Comenzar");
                running = !running;
            }
        }
    }

}
