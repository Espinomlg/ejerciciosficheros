package com.example.espino.ejerciciosficheros;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class LaunchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
    }

    public void launchActivity(View view){

        switch (view.getId()){

            case R.id.launch_btn1:
                startActivity(new Intent(LaunchActivity.this, DiaryActivity.class));
                break;

            case R.id.launch_btn2:
                startActivity(new Intent(LaunchActivity.this,AlarmsActivity.class));
                break;

            case R.id.launch_btn3:
                startActivity(new Intent(LaunchActivity.this,FertilDaysActivity.class));
                break;

            case R.id.launch_btn4:
                startActivity(new Intent(LaunchActivity.this,WebBrowserActivity.class));
                break;

            case R.id.launch_btn5:
                startActivity(new Intent(LaunchActivity.this,DownloadImageActivity.class));
                break;

            case R.id.launch_btn6:
                startActivity(new Intent(LaunchActivity.this,DivisasActivity.class));
                break;

            case R.id.launch_btn7:
                startActivity(new Intent(LaunchActivity.this, UploadActivity.class));
                break;
        }
    }
}
