package com.example.espino.ejerciciosficheros.models;


import android.content.Context;

import com.example.espino.ejerciciosficheros.fileMethods.FileInputMethods;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;


public class Diary implements Serializable {

    private ArrayList<Contact> diary;
    private static Diary diarySingleton;
    private static File file;
    private FileInputMethods fim;
    private Context context;

    private Diary(Context context){
        this.context = context;
        fim = new FileInputMethods(context);
        file = new File(context.getFilesDir(),Files.DIARY_FILE);

        if(file.length() > 0) {
            diary = (ArrayList<Contact>) fim.readInternal(Files.DIARY_FILE);
        }
        else
            diary = new ArrayList<>();

    }

    public static Diary getInstance(Context context){
        if(diarySingleton == null)
            diarySingleton = new Diary(context);

        return diarySingleton;
    }

    public void add(Contact contact){
        diary.add(contact);

    }

    public Contact getItem(int index){
        return diary.get(index);
    }

    public ArrayList<Contact> getDiary(){
        return diary;
    }
}
