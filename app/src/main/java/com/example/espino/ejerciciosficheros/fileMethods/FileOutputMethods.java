package com.example.espino.ejerciciosficheros.fileMethods;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;



public class FileOutputMethods {

    private Context context;

    public FileOutputMethods(Context context){
        this.context = context;
    }

    private boolean write(File file, Object object, boolean append, String charset){

        boolean ok = true;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);

            oos.writeObject(object);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }

        return ok;
    }

    public boolean writeInternal(String fileName, Object object, boolean append, String charset){
        File file = new File(context.getFilesDir(), fileName);

        return write(file, object, append, charset);

    }

    public String showInternalProperties(String file){

        File myFile = new File(context.getFilesDir(),file);

        return showProperties(myFile);
    }

    public boolean writeExternal(String file, Object object, Boolean add, String charset) {
        File myFile, sdcard;
        sdcard = Environment.getExternalStorageDirectory();
        //tarjeta = Environment.getExternalStoragePublicDirectory("datos/programas/");
        //tarjeta.mkdirs();
        myFile = new File(sdcard.getAbsolutePath(), file);

        return write(myFile,object,add,charset);
    }

    private String showProperties(File file){
        SimpleDateFormat dateFormat = null;
        StringBuffer txt = new StringBuffer();

        try{
            if(file.exists()){
                dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss", Locale.getDefault());
                txt.append("Nombre: " + file.getName() + "\n" +
                        "Ruta: " + file.getAbsolutePath() + "\n" +
                        "Tamaño (bytes): " + Long.toString(file.length()) + "\n" +
                        "Ultima modificación: " + dateFormat.format(new Date(file.lastModified()) + "\n"));
            }else
                txt.append("No existe el fichero " + file.getName() + "\n");

        }catch(Exception e){
            Log.e("Error", e.getMessage());
            txt.append(e.getMessage());
        }

        return txt.toString();

    }

    public boolean externalStorageAvailable(){
        boolean exists = false;

        //Comprobamos el estado de la memoria externa (tarjeta SD)
        String state = Environment.getExternalStorageState();

        if (state.equals(Environment.MEDIA_MOUNTED))
            exists = true;

        return exists;
    }




}
