package com.example.espino.ejerciciosficheros.models;


import java.io.Serializable;

public class PeriodDate implements Serializable{

    private String lastDay;

    private int duration;

    public PeriodDate(String lastDay, int duration) {
        this.lastDay = lastDay;
        this.duration = duration;
    }



    public String getLastDay() {
        return lastDay;
    }

    public void setLastDay(String lastDay) {
        this.lastDay = lastDay;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
